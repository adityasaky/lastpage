---
title: It was the City
---

<span class="author-note">AN: I think I first wrote this when we were learning about the Indus Valley Civilisation. I was fascinated by Harappa, and Mohenjo Daro and I spent ages imagining what life must have been like back then.</span>

It was the City. It had a name but that name has long been forgotten. It's no loss as the name doesn't really matter to us too much. Suffice it to say, it was the City. People came from all over. Some came to escape the starvation that would have claimed them back home. Others came for it was the city of opportunities. And some came as travellers out to see the world, but settled and made the City their home.

It was the City. Like most cities, it was clogged with people. There were the Upper areas, where the rich lived, where the houses were actually mansions, and where the lanes were wider. But much of the City was not so fortunate. The Lower areas had ten people living in the space occupied by one in the Upper areas.

It was the City. Like every city, it had its markets. Some were frequented by the Upper-dwellers. These markets were visibly wealthy, the fortune of the traders made by their patrons. These traders dealt in expensive goods. Their children were educated and rarely followed their fathers' footsteps into trade. But there were more markets frequented by the Lower-dwellers. The goods one could buy here were no different from the Upper markets but they cost a lot lesser. Thus, the traders were not well off and their children, instead of gaining an education, spent their childhood helping with the trade, until they were old enough to take it up themselves.

It was the City. Like every city, it had a ruler. And like most rulers, this one had a council of wise men and women. They ruled the city fairly, at least in their skewed perspective. Ruler succeeded ruler and many wise men and women ascended to the council to take the place of those who vacated, but the lives of the people didn't really change. Soon, the cynicism set in and many stopped caring who ruled them. Politics became a sport of the rich.

It was the City. Until it wasn't. The City endured. Until it didn't. The wise men and women thought that it would last forever. Perhaps, they weren't so wise after all. And so the city faded. But soon, another rose to take its place. And so the cycle continues. There will always be the City.
