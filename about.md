---
title: What is Last Page?
permalink: /what-is-last-page/
---

I'm Aditya Saky and I occasionally write. I didn't want anything I write on my main blog <a href="http://saky.in" target="_blank">saky.in</a>.

I used to scribble out stories, small essays, and my thoughts in general on the last page of my notebooks back in school. Over the years, many of these notebooks have since been sold for scrap. What I still have, I plan to upload here.
